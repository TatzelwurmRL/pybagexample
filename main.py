import pygame
import asyncio
import random

async def main():
    pygame.init()
    pygame.mixer.init()

    screen = pygame.display.set_mode((800, 600), vsync=1)
    r, g, b = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)

    pygame.mixer.music.load('stg_st001-intro.ogg')
    pygame.mixer.music.play(loops=0)
    pygame.mixer.music.queue('stg_st001-intro.ogg', loops=-1)

    while True:
        events = pygame.event.get()
        for e in events:
            if e.type == pygame.QUIT: return
        
        r = r + 1 if r < 255 else 0
        g = g + 1 if g < 255 else 0
        b = b + 1 if b < 255 else 0
        screen.fill((r, g, b))
        
        pygame.display.flip()
        await asyncio.sleep(0)
asyncio.run(main())